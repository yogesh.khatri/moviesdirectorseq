const moviesData = require("./movies")

let count = 1;
let directorObj = {};

// getting data in object for unique id
for (let i = 0; i < moviesData.length; i++) {
    if (moviesData[i]["Director"] in directorObj) {
        continue;
    }
    else {
        directorObj[moviesData[i]["Director"]] = count;
        count += 1;
    }
}

// getting data in director list for insertion in director Table
let directorList = [];
for (let director in directorObj) {
    let temp = {};
    temp['director'] = director;
    directorList.push(temp);
}
// creating movies list
let moviesList = [];
for (let i = 0; i < moviesData.length; i++) {
    let temp = {};
    for (let field in moviesData[i]) {
        if (field == "Director") {
            // do nothing
        } else {
            temp[field] = moviesData[i][field]
        }
    }
    temp['director_id'] = directorObj[moviesData[i]["Director"]];
    moviesList.push(temp)
}

module.exports = {
    directorList,
    moviesList
}