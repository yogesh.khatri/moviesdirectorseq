'use strict';
module.exports = (sequelize, DataTypes) => {
  const movies = sequelize.define('movies', {
    id: {
      type: DataTypes.INTEGER(12),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    rank: DataTypes.INTEGER,
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    runtime: DataTypes.INTEGER,
    genre: DataTypes.STRING,
    rating: DataTypes.INTEGER,
    metascore: DataTypes.STRING,
    votes: DataTypes.INTEGER,
    gross_earning_in_mil: DataTypes.STRING,
    actor: DataTypes.STRING,
    year: DataTypes.INTEGER,
    director_id: DataTypes.INTEGER
  }, {
    timestamps: false
  });

  movies.associate = function (models) {
    movies.belongsTo(models.directors, { foreignKey: 'id' });
  };
  return movies;
};