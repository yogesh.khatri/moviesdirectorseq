'use strict';
module.exports = (sequelize, DataTypes) => {
  const directors = sequelize.define('directors', {
    id: {
      type: DataTypes.INTEGER(12),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    director: DataTypes.STRING
  }, {
    timestamps: false
  });
  directors.associate = function (models) {
    // associations can be defined here
  };
  return directors;
};