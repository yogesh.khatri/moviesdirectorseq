'use strict';
const Sequelize = require('sequelize');

const data = require('../utils/insertingData.js');
// console.log(data.directorList)
// console.log(data.moviesList)
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('movies', data.moviesList)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('movies', null, {});
  }
};
