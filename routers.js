var express = require('express');
var router = express.Router();
var db = require('./models');
var bodyParser = require('body-parser')
var logger = require('./config/winstonConfig');
var directors = db.directors;
var movies = db.movies;
var fs = require('fs')
var path = require('path')

// importing  morgan
var morgan = require('morgan')
var accessLogStream = fs.createWriteStream(path.join(__dirname, './logs/access.log'), { flags: 'a' })
router.use(morgan('common', { stream: accessLogStream }))

// using  body-parser
router.use(bodyParser.json())

// Get all movies
router.get('/movies', (req, res) => {
    movies.findAll().
        then((movies) => {
            if (movies) {
                res.send(movies);
            }
            else {
                throw new Error("Movies not found")
            }
        })
        .catch((err) => {
            logger.error(err);
            res.status(404).json(err.message)
        })
})

// Get all Directors
router.get('/directors', (req, res) => {
    directors.findAll()
        .then((directors) => {
            if (directors === null) {
                console.log('Not found!');
                res.status(400).send("Bad Request")
                throw new Error("Movies not found")
            } else {
                res.send(directors)
            }
        })
        .catch((err) => {
            logger.error(err);
            res.status(404).json(err.message)
        })
})

// Get movies by ID
router.get('/movies/:id', (req, res) => {
    movies.findByPk(req.params.id)
        .then((movie) => {
            if (movie) {
                res.status(200).json(movie)
            }
            else {
                throw new Error('Movie not Found');
            }
        })
        .catch((err) => {
            logger.error(err)
            res.status(404).json(err.message)
        })
})

// Get directors by ID
router.get('/directors/:id', (req, res) => {
    directors.findByPk(req.params.id)
        .then((director) => {
            if (director) {
                res.status(200).json(director)
            }
            else {
                throw new Error('Director not Found');
            }
        })
        .catch((err) => {
            logger.error(err)
            res.status(404).json(err.message)
        })
})
// Adding New director
router.post('/directors', (req, res) => {
    directors.create({
        director: req.body.director,
    })
        .then(function (director) {
            res.send(`director "${req.body.director}" added successfuly`)
        })
        .catch((err) => {
            logger.error(err);
            res.status(400).send("Invalid Syntex")

        })
});

// Adding New Movie
router.post('/movies', (req, res) => {
    movies.create({
        rank: (req.body.rank == undefined) ? null : req.body.rank,
        title: req.body.title,
        description: (req.body.description == undefined) ? null : req.body.description,
        runtime: (req.body.runtime == undefined) ? null : req.body.runtime,
        genre: (req.body.genre == undefined) ? null : req.body.genre,
        rating: (req.body.rating == undefined) ? null : req.body.rating,
        metascore: (req.body.metascore == undefined) ? null : req.body.metascore,
        gross_earning_in_mil: (req.body.gross_earning_in_mil == undefined) ? null : req.body.gross_earning_in_mil,
        actor: (req.body.actor == undefined) ? null : req.body.actor,
        year: (req.body.year == undefined) ? null : req.body.year,
        director_id: (req.body.director_id == undefined) ? null : req.body.director_id,

    })
        .then(function (movie) {
            res.json(movie);
        })
        .catch((err) => {
            res.status(400).send("Invalid Syntex")
            logger.error(err);
        })
});

// Delete a director
router.delete('/directors/:id', function (req, res) {
    movies.destroy({
        where: {
            director_id: req.params.id
        }
    })
        .then((movie) => {
            if (movie == 0) {
                throw new Error('Id does Not Exists')
            }

            else {
                directors.destroy({
                    where: {
                        id: req.params.id
                    }
                })
            }
        })
        .catch((err) => {
            logger.error(err);
            res.status(404).send("director does not exist");
        })
        .then((director) => {
            console.log(director + "--------------------------------------");
            if (director == 0) {
                res.status(404).send("director does not exist");
                throw new Error('Director not Found');
            }
            else {
                res.send(`director deleted successfuly`)

            }
        })
        .catch((err) => {
            logger.error(err);
        })
});

// Delete a movie
router.delete('/movies/:id', (req, res) => {
    movies.destroy({
        where: {
            id: req.params.id
        }
    })
        .then(function (movie) {
            if (movie == 0) {
                res.status(404).send("movie does not exist");
                throw new Error('movie not Found');

            }
            else {
                res.send(`movie deleted successfuly`)
            }
        })
        .catch((err) => {
            logger.error(err);
        })
});

// Update a director
router.put("/directors/:id", (req, res) => {
    directors.sequelize.query(`update directors set  director="${req.body.director}" where id=${req.params.id}`)
        .then((results) => {
            if (results.affectedRows == 0) {
                res.status(400).send("No changes are made!! Try again");
                throw new Error('Director not Found');
            }
            else {
                res.send(`director updated `)
            }
        })
        .catch((err) => {
            logger.error(err);

        })
})

// Update a movies
router.put("/movies/:id", async function (req, res) {
    let sql = `update movies  set `;
    for (let key in req.body) {
        sql += ` ${key}="${req.body[key]}", `
    }
    sql = sql.slice(0, -2);
    sql += ` where id=${req.params.id};`;

    // const [results, metadata] = await movies.sequelize.query(sql);
    movies.sequelize.query(sql)
        .then((results) => {
            if (results.affectedRows == 0) {
                res.status(400).send("No changes are made!! Try again");
                throw new Error('movie not Found');

            }
            else {
                res.send(`movie updated `);
            }
        })
        .catch((err) => {
            logger.error(err);

        })

})

module.exports = router;