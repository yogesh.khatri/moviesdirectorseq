var express = require('express');
var db = require('./models');
var router = require('./routers');
var app = express();

// homepage
app.get('/', (req, res) => {
  res.send('Hello World!');
});

// using router
app.use('', router);

// starting server at 3000 port
app.listen(3000, function () {
  console.log("server started at 3000")
  db.sequelize.sync();
});
